One time commands-> 
sudo apt-get install git-core    (installing git)
git init                         (initializing git)

Use commands->
git clone "url"			(to clone a repository)
git status			(gives status of files)
git add	filename		(to add a file we want link with our repository)
git commit -m "message" filename(to commit a file)
git push origin master		(to push the changes or newfile into our repository)
git pull origin master		(to pull again our repository with all the changes that has been made)
git checkout filename		(remove changes and conflicts of a file)
