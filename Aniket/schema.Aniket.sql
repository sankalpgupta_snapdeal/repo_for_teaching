-- 
-- Created by SQL::Translator::Producer::PostgreSQL
-- Created on Mon Aug 27 10:52:37 2012
-- 
--
-- Table: customer
--
CREATE TABLE "customer" (
  "cusid" serial NOT NULL,
  "name" character varying(255) DEFAULT NULL,
  "address" text,
  "phoneNum" character varying(20) DEFAULT NULL,
  "emailId" character varying(50) DEFAULT NULL,
  PRIMARY KEY ("cusid")
);

--
-- Table: employee
--
CREATE TABLE "employee" (
  "empid" serial NOT NULL,
  "name" character varying(50) DEFAULT NULL,
  "designation" character varying(50) DEFAULT NULL,
  "department" character varying(50) DEFAULT NULL,
  "joinDate" date DEFAULT NULL,
  "contactNum" character varying(20) DEFAULT NULL,
  "address" text,
  PRIMARY KEY ("empid")
);

--
-- Table: itemStockMap
--
CREATE TABLE "itemStockMap" (
  "itemid" bigint DEFAULT NULL,
  "stockid" bigint DEFAULT NULL
);
CREATE INDEX "fk_itemStock" on "itemStockMap" ("itemid");
CREATE INDEX "fk_stockItem" on "itemStockMap" ("stockid");

--
-- Table: items
--
CREATE TABLE "items" (
  "itemid" serial NOT NULL,
  "name" character varying(50) NOT NULL,
  "price" numeric(7,2) DEFAULT 0.00 NOT NULL,
  "category" character varying(14) NOT NULL,
  PRIMARY KEY ("itemid")
);

--
-- Table: orders
--
CREATE TABLE "orders" (
  "itemid" bigint DEFAULT NULL,
  "orderid" bigint DEFAULT NULL,
  "quantity" bigint DEFAULT 1,
  "amount" numeric(8,2) DEFAULT 0.00
);
CREATE INDEX "fk_orderItems" on "orders" ("itemid");
CREATE INDEX "fk_order" on "orders" ("orderid");

--
-- Table: parentOrder
--
CREATE TABLE "parentOrder" (
  "orderid" serial NOT NULL,
  "cusid" bigint DEFAULT NULL,
  "category" character varying(13) NOT NULL,
  "empid" bigint DEFAULT NULL,
  "performance" character varying(13) DEFAULT NULL,
  "orderTime" timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  "deliveryStatus" character varying(14) NOT NULL,
  "totalAmount" numeric(8,2) DEFAULT 0.00,
  PRIMARY KEY ("orderid")
);
CREATE INDEX "fk_orderCus" on "parentOrder" ("cusid");
CREATE INDEX "fk_empOrder" on "parentOrder" ("empid");

--
-- Table: stock
--
CREATE TABLE "stock" (
  "stockid" serial NOT NULL,
  "name" character varying(50) NOT NULL,
  "currentStock" character varying(15) DEFAULT '0',
  "stockDemand" character varying(15) DEFAULT NULL,
  "expectedDate" timestamp DEFAULT NULL,
  PRIMARY KEY ("stockid")
);

--
-- Table: stockVendorMap
--
CREATE TABLE "stockVendorMap" (
  "stockid" bigint DEFAULT NULL,
  "vendorid" bigint DEFAULT NULL
);
CREATE INDEX "fk_stockVendor" on "stockVendorMap" ("stockid");
CREATE INDEX "fk_vendorStock" on "stockVendorMap" ("vendorid");

--
-- Table: vendors
--
CREATE TABLE "vendors" (
  "vendorid" serial NOT NULL,
  "name" character varying(50) NOT NULL,
  "contactNum" character varying(20) DEFAULT NULL,
  "contactinfo" text,
  PRIMARY KEY ("vendorid")
);

--
-- Foreign Key Definitions
--

ALTER TABLE "itemStockMap" ADD FOREIGN KEY ("stockid")
  REFERENCES "stock" ("stockid") DEFERRABLE;

ALTER TABLE "itemStockMap" ADD FOREIGN KEY ("itemid")
  REFERENCES "items" ("itemid") DEFERRABLE;

ALTER TABLE "orders" ADD FOREIGN KEY ("orderid")
  REFERENCES "parentOrder" ("orderid") DEFERRABLE;

ALTER TABLE "orders" ADD FOREIGN KEY ("itemid")
  REFERENCES "items" ("itemid") DEFERRABLE;

ALTER TABLE "parentOrder" ADD FOREIGN KEY ("empid")
  REFERENCES "employee" ("empid") DEFERRABLE;

ALTER TABLE "parentOrder" ADD FOREIGN KEY ("cusid")
  REFERENCES "customer" ("cusid") DEFERRABLE;

ALTER TABLE "stockVendorMap" ADD FOREIGN KEY ("vendorid")
  REFERENCES "vendors" ("vendorid") DEFERRABLE;

ALTER TABLE "stockVendorMap" ADD FOREIGN KEY ("stockid")
  REFERENCES "stock" ("stockid") DEFERRABLE;

