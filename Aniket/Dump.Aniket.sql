-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: aniket
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `cusid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` text,
  `phoneNum` varchar(20) DEFAULT NULL,
  `emailId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cusid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'Aniket Patni','qwer','+91-9343493','asdnd@dsadfo.com');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `empid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `joinDate` date DEFAULT NULL,
  `contactNum` varchar(20) DEFAULT NULL,
  `address` text,
  PRIMARY KEY (`empid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'abcd','Waiter','Serving','0000-00-00','+91-9329332','zxcv');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemStockMap`
--

DROP TABLE IF EXISTS `itemStockMap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemStockMap` (
  `itemid` int(11) DEFAULT NULL,
  `stockid` int(11) DEFAULT NULL,
  KEY `fk_itemStock` (`itemid`),
  KEY `fk_stockItem` (`stockid`),
  CONSTRAINT `fk_stockItem` FOREIGN KEY (`stockid`) REFERENCES `stock` (`stockid`),
  CONSTRAINT `fk_itemStock` FOREIGN KEY (`itemid`) REFERENCES `items` (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemStockMap`
--

LOCK TABLES `itemStockMap` WRITE;
/*!40000 ALTER TABLE `itemStockMap` DISABLE KEYS */;
INSERT INTO `itemStockMap` VALUES (2,1);
/*!40000 ALTER TABLE `itemStockMap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `itemid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` decimal(7,2) NOT NULL DEFAULT '0.00',
  `category` enum('prepared items','packed items') NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Omlette',45.00,'prepared items'),(2,'Cold-Milk',25.00,'packed items');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `itemid` int(11) DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT '1',
  `amount` decimal(8,2) DEFAULT '0.00',
  KEY `fk_orderItems` (`itemid`),
  KEY `fk_order` (`orderid`),
  CONSTRAINT `fk_order` FOREIGN KEY (`orderid`) REFERENCES `parentOrder` (`orderid`),
  CONSTRAINT `fk_orderItems` FOREIGN KEY (`itemid`) REFERENCES `items` (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,1,45.00),(2,1,2,50.00);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parentOrder`
--

DROP TABLE IF EXISTS `parentOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parentOrder` (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `cusid` int(11) DEFAULT NULL,
  `category` enum('home-delivery','dining') NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `performance` enum('satisfied','non-satisfied') DEFAULT NULL,
  `orderTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deliveryStatus` enum('not-delivered','delivered','in-process','not-applicable') NOT NULL,
  `totalAmount` decimal(8,2) DEFAULT '0.00',
  PRIMARY KEY (`orderid`),
  KEY `fk_orderCus` (`cusid`),
  KEY `fk_empOrder` (`empid`),
  CONSTRAINT `fk_empOrder` FOREIGN KEY (`empid`) REFERENCES `employee` (`empid`),
  CONSTRAINT `fk_orderCus` FOREIGN KEY (`cusid`) REFERENCES `customer` (`cusid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parentOrder`
--

LOCK TABLES `parentOrder` WRITE;
/*!40000 ALTER TABLE `parentOrder` DISABLE KEYS */;
INSERT INTO `parentOrder` VALUES (1,1,'dining',1,'satisfied','2012-08-27 00:18:13','not-applicable',95.00);
/*!40000 ALTER TABLE `parentOrder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `stockid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `currentStock` varchar(15) DEFAULT '0',
  `stockDemand` varchar(15) DEFAULT NULL,
  `expectedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`stockid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock`
--

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` VALUES (1,'Milk','3 Litres','10 Litres','2012-08-28 00:00:00'),(2,'Eggs','2 packs','1 Pack','2012-08-28 00:00:00'),(3,'Bread','2 packs','0','2012-08-28 00:00:00'),(4,'Tomatoes','2 kg','0','2012-08-30 00:00:00'),(5,'Amul Butter','5 kg','0','2012-08-31 00:00:00');
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stockVendorMap`
--

DROP TABLE IF EXISTS `stockVendorMap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockVendorMap` (
  `stockid` int(11) DEFAULT NULL,
  `vendorid` int(11) DEFAULT NULL,
  KEY `fk_stockVendor` (`stockid`),
  KEY `fk_vendorStock` (`vendorid`),
  CONSTRAINT `fk_vendorStock` FOREIGN KEY (`vendorid`) REFERENCES `vendors` (`vendorid`),
  CONSTRAINT `fk_stockVendor` FOREIGN KEY (`stockid`) REFERENCES `stock` (`stockid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stockVendorMap`
--

LOCK TABLES `stockVendorMap` WRITE;
/*!40000 ALTER TABLE `stockVendorMap` DISABLE KEYS */;
INSERT INTO `stockVendorMap` VALUES (1,1),(2,1),(3,1),(3,2),(1,2),(5,2);
/*!40000 ALTER TABLE `stockVendorMap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `vendorid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `contactNum` varchar(20) DEFAULT NULL,
  `contactinfo` text,
  PRIMARY KEY (`vendorid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendors`
--

LOCK TABLES `vendors` WRITE;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
INSERT INTO `vendors` VALUES (1,'lmnop','+91-83283','qwerasdf'),(2,'Global Dairy','4321213','asdfghjkl');
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-27  6:15:59
